#  IF YOU WANT SOMETHING TO BE AVAILABLE FOR BOTH LOGIN
#  AND NON-LOGIN INTERACTIVE SHELLS, PUT IT IN HERE.
#
# Bash initialization cheat sheet.  Read down the appropriate
# column. Executes A, then B, then C, etc. The B1, B2, B3 means it
# executes only the first of those files found.
#
# +----------------+-----------+-----------+------+
# |                |Interactive|Interactive|Script|
# |                |login      |non-login  |      |
# +----------------+-----------+-----------+------+
# |/etc/profile    |   A       |           |      |
# +----------------+-----------+-----------+------+
# |/etc/bash.bashrc|           |    A      |      |
# +----------------+-----------+-----------+------+
# |~/.bashrc       |           |    B      |      |
# +----------------+-----------+-----------+------+
# |~/.bash_profile |   B1      |           |      |
# +----------------+-----------+-----------+------+
# |~/.bash_login   |   B2      |           |      |
# +----------------+-----------+-----------+------+
# |~/.profile      |   B3      |           |      |
# +----------------+-----------+-----------+------+
# |BASH_ENV        |           |           |  A   |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |~/.bash_logout  |    C      |           |      |
# +----------------+-----------+-----------+------+
#
# By default bash reads .bashrc *only* for interactive non-login 
# shells. Login shells read *only* .bash_profile.  Since I
# typically wish to share much between both login and non-login
# interactive shells, *my* .bash_profile sources in .bashrc.
# So, the net effect is that *my* .bashrc is read by both login
# and non-login (i.e., *all* interactive) shells.
#

function configure_bash() {

    # At end of session, append history to histfile, rather than replace.
    shopt -s histappend

    export HISTSIZE=5000
    export HISTFILESIZE=5000

    # Note: Enabling HISTTIMEFORMAT disables per-session shell history
    export HISTTIMEFORMAT='%F %T '

    # Ignore duplicate commands
    export HISTCONTROL=ignoredups

    # Don't place matching commands in history
    export HISTIGNORE='bg:fg:history'

    # Save to histfile for each command, rather than save all at end of session
    PROMPT_COMMAND="history -a${PROMPT_COMMAND:+; $PROMPT_COMMAND}"

    # 'r' will rerun command matching arg.  E.g., 'r cc', reruns last 'cc'.
    alias r='fc -s'

    # TODO - probably don't want this in non-login shell!
    # EOF (^d) won't cause shell to exit
    export IGNOREEOF=10
    
    alias ls='ls -CF'
}

configure_bash


################################################################################
#
# A few basic tools
#
################################################################################

source_if_present () {
    [[ -f "$1" ]] && source "$1"
}

pathappend() {
    # Add $1 to the end of $PATH, provided that the directory
    # exists and that $1 is not already in $PATH.
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${PATH:+"$PATH:"}$1"
    fi
}

pathprepend() {
    # Add $1 to the beginning of $PATH, provided that the directory
    # exists and that $1 is not already in $PATH.
    if [ -d "$1" ] && [[ ":$PATH:" != *":$1:"* ]]; then
        PATH="${1}${PATH:+":$PATH"}"
    fi
}

################################################################################
################################################################################
#
# $PATH strategy:
#
# 1) Want my local ~/bin to be first in the path so that it is easy to override
# behavior by putting stuff there.  This can be any type of script or binary.
#
# 2) Then want the virtualenv currently in effect to be next in
# precedence.  This is so that it can override anything done by
# MacPorts of by the default system configuration of OS-X.  This only
# affects python behavior -- there should not be any scripts or
# binaries in the virtualenv path that are not python related.
# 
# 3) Next in precedence is the path to MacPorts.  This can be any sort
# of script or binary.  These can either be: a) things that don't come
# with OS-X that I have added via MacPorts, or b) things I've added
# via MacPorts that are newer/ different from a similar thing included
# with OS-X.  Either way, these items should be earlier in the path
# than the default OS-X stuff.
# 
# 4) The default $PATH as set by OS-X.
#
################################################################################
################################################################################

function configure_macports() {
    pathprepend /opt/local/bin
    pathprepend /opt/local/sbin
}

configure_macports

# N.B.: Do this *after* configure_macports 
# or anything else that modifies the front of PATH.
pathprepend ~/bin

# Source files in the given directory
source_dir()
{
    local dir="$1"
    if [[ -d $dir ]]; then
        local conf_file
        for conf_file in "$dir"/* ; do
            source "$conf_file"
        done
    fi
}

source_dir ~/.bash.d/init

# Set path for Haskell / ghc, as well as locally built Haskell programs.
# cabal places executables in ~/Library/Haskell/bin
# stack places executables in ~/.local/bin
PATH=~/.local/bin:~/Library/Haskell/bin:/Library/Haskell/bin:$PATH

PATH="/Users/khe/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/Users/khe/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/Users/khe/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/Users/khe/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/Users/khe/perl5"; export PERL_MM_OPT;

PATH=~/.npm-global/bin:$PATH


source /Users/khe/Library/Preferences/org.dystroy.broot/launcher/bash/br
