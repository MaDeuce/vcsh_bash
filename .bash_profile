# Read by login shells only.

export PROMPT_COMMAND=''

# This function shortens a path so that it is no longer than a specified length.
# Intended to trim paths that will be included in PS1
_PS1 ()
{
    local PRE= NAME="$1" LENGTH="$2";
    [[ "$NAME" != "${NAME#$HOME/}" || -z "${NAME#$HOME}" ]] &&
        PRE+='~' NAME="${NAME#$HOME}" LENGTH=$[LENGTH-1];
    ((${#NAME}>$LENGTH)) && NAME="/...${NAME:$[${#NAME}-LENGTH+4]}";
    echo "$PRE$NAME"
}
# Will show $PWD in the prompt to a max len of 20 characters.
export PS1='[\u@\h]$(_PS1 "$PWD" 20) \$ '
# export PS1='[\u@\H]\w> '


if [ -r ~/.bashrc ]
then
    # Read in non-interactive setup first
    # Currently, some of these modify PS1.  Yes, I know, a non-sequitir
    # when combined with non-interactive...
    source ~/.bashrc
fi

# Now, interactive-only setup

whereisfunc() (
    # Show exactly where a function is defined.
    shopt -s extdebug
    declare -F $1
    shopt -u extdebug
)

ret() {
    # Remove Emacs Turds -- Remove any file whose name ends with a '~'.
    for file in ./*~
    do
	[[ -e "$file" ]] && rm "$file"
    done
}

# reset xterm window to 24x80
alias rs='printf "\e[8;24;80t"'

# clear the terminal
alias cls=clear

source /etc/bashrc_Apple_Terminal

source /Users/khe/Library/Preferences/org.dystroy.broot/launcher/bash/br
